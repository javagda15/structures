package pl.sda.structures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Benchmark {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();

        for (int j = 0; j <10; j++) {
            list.clear();
            Long startTime = System.currentTimeMillis();

            ///
            for (int i = 0; i < 1000000; i++) {
                list.add(0, i);
            }

            ///

            System.out.println(System.currentTimeMillis() - startTime);



        }
    }
}
